//
//  GameScene.m
//  p04-Desu
//
//  Created by Anuroop Desu on 2/17/16./Users/adesu1/Desktop/asteroid.png
//  Copyright (c) 2016 Anuroop Desu. All rights reserved.
//

#import "GameScene.h"
#import "FMMParallaxNode.h"
@import AVFoundation;
#define ARC4RANDOM_MAX      0x10000000
@implementation GameScene
{
    
    SKSpriteNode *ship;
    SKLabelNode *myLabel;
    SKLabelNode *scoreLabel;
    SKSpriteNode *fireButtonL;
    SKSpriteNode* fireButtonR;
    NSMutableArray *bullets;
    NSMutableArray *asteroids;
    NSMutableArray *shiplives;
    SKNode *checkFireObject;
    
    int next_bullet, lives, tscore;
    int next_asteroid, next_asteroid_spawn;
    
    FMMParallaxNode *Backgrounds;
    FMMParallaxNode *SpaceDust;
    //for adding background music and sounds
    AVAudioPlayer *_backgroundAudioPlayer;
}

-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:@"space_new.jpg"];
    background.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame));
    background.size = self.frame.size;
    background.zPosition = -1;
    [self addChild:background];
    self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
    myLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    
    myLabel.text = @"Touch to Play!";
    myLabel.fontSize = 45;
    myLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                   CGRectGetMidY(self.frame));
    
    [self addChild:myLabel];
    
}


-(void)moveShip {
    CGPoint p = ship.position;
    
    float touchX = [[shipTimer userInfo] floatValue];
    
    if (touchX - p.x > 0) {
        p.x += 10;
    } else if ((touchX - p.x < 0)) {
        p.x -= 10;
    }
    
    if (p.x + (ship.size.width / 2) > self.size.width) {
        p.x = self.size.width - (ship.size.width / 2);
    } else if (p.x - (ship.size.width / 2) < 0) {
        p.x = (ship.size.width / 2);
    }
    p.y = 175;
    
    ship.position = p;
    
    if (fabs(p.x - touchX) < 5) {
        touchesBegan = NO;
        [shipTimer invalidate];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    for (UITouch *touch in touches) {
        [myLabel removeFromParent];
        CGPoint location = [touch locationInNode:self];
        if(ship.parent == NULL) {
            lives = 3;
            ship = [SKSpriteNode spriteNodeWithImageNamed:@"Spaceship"];
            ship.name =@ "ship";
            ship.xScale = 0.25;
            ship.yScale = 0.25;
            //SKAction *rotation = [SKAction rotateByAngle: M_PI duration:5.0];
            ship.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(ship.size.width, ship.size.height)];
            location.y = 175;
            location.x = (self.size.width / 2);
            ship.position = location;
            ship.physicsBody.dynamic = YES;
            ship.physicsBody.affectedByGravity = NO;
            ship.physicsBody.mass = 0.20;
            [self addChild:ship];
            
            //Adding bullets to screen
            bullets = [[NSMutableArray alloc]initWithCapacity:5];
            for(int i=0;i<25;i++) {
                SKSpriteNode *bullet = [SKSpriteNode spriteNodeWithImageNamed:@"bluelaser.jpeg"];
                //size:CGSizeMake(4, 40)
                bullet.hidden = YES;
                [bullets addObject:bullet];
                [self addChild:bullet];
            }
            next_bullet = 0;
            
            //Adding Asteroids to Screen
            asteroids = [[NSMutableArray alloc]initWithCapacity:5];
            for(int i=0;i<100;i++){
                SKSpriteNode *asteroid = [SKSpriteNode spriteNodeWithImageNamed:@"asteroid.png"];
                asteroid.xScale = 0.45;
                asteroid.yScale = 0.45;
                asteroid.hidden = YES;
                [asteroids addObject:asteroid];
                [self addChild:asteroid];
            }
            next_asteroid = tscore = 0;
            next_asteroid_spawn= 0;
            lives = 3;
            
            shiplives = [[NSMutableArray alloc] init];
            for(int i=0;i<lives;i++) {
                SKSpriteNode *shiplive = [SKSpriteNode spriteNodeWithImageNamed:@"Spaceship"];
                location.y = 630;
                location.x = 50 + (40*i);
                shiplive.position = location;
                shiplive.xScale = 0.10;
                shiplive.yScale = 0.10;
                [shiplives addObject:shiplive];
                [self addChild:shiplive];
            }
            scoreLabel = [SKLabelNode labelNodeWithFontNamed:@"American Typewriter"];
            scoreLabel.fontSize = 25;
            scoreLabel.position = CGPointMake(950,630);
            [scoreLabel setText:[NSString stringWithFormat:@"Score: %d", tscore]];
            [self addChild:scoreLabel];
            
            //adding fire buttons
            //left fire button
            fireButtonL = [SKSpriteNode spriteNodeWithImageNamed:@"redbutton.png"];
            fireButtonL.xScale = 0.10;
            fireButtonL.yScale = 0.10;
            fireButtonL.name = @"fireButtonL";
            CGPoint fireButtonLocationL = CGPointMake(50, self.size.height / 4);
            fireButtonL.position = fireButtonLocationL;
            [self addChild:fireButtonL];
            
            //right fire button
            fireButtonR = [SKSpriteNode spriteNodeWithImageNamed:@"redbutton.png"];
            fireButtonR.xScale = 0.1;
            fireButtonR.yScale = 0.1;
            fireButtonR.name = @"fireButtonR";
            CGPoint fireButtonLocationR = CGPointMake(self.size.width-50, self.size.height / 4);
            fireButtonR.position = fireButtonLocationR;
            [self addChild:fireButtonR];
        }
        else {
             touchesBegan = YES;
             CGPoint p = [touch locationInNode:self];
            checkFireObject = [self nodeAtPoint:location];
            if ([checkFireObject.name isEqualToString:@"fireButtonR"]) {
                SKAction *colorChage = [SKAction colorizeWithColor:[SKColor redColor] colorBlendFactor:.5 duration:.001];
                [fireButtonR runAction:colorChage];
                fireRPressed = YES;
                [self fireBullets:0];
            } else if ([checkFireObject.name isEqualToString:@"fireButtonL"]) {
                SKAction *colorChage = [SKAction colorizeWithColor:[SKColor redColor] colorBlendFactor:.5 duration:.001];
                [fireButtonL runAction:colorChage];
                fireLPressed = YES;
                [self fireBullets:1];
            }else {
                 [shipTimer invalidate];
                 if (fabs(p.x - ship.position.x) > 5) {
                     NSString *direction = [NSString stringWithFormat:@"%f", p.x];
                     shipTimer = [NSTimer scheduledTimerWithTimeInterval:.0166 target:self selector:@selector(moveShip) userInfo:direction repeats:YES];
                 } else {
                     touchesBegan = NO;
                 }
            }
        }
    }
}
                              
-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    for (UITouch *t in touches)
    {
        if (!touchesBegan) {
            CGPoint p = [t locationInNode:self];
            if (p.x + (ship.size.width / 2) > self.size.width) {
                p.x = self.size.width - (ship.size.width / 2);
            } else if (p.x - (ship.size.width / 2) < 0) {
                p.x = (ship.size.width / 2);
            }
            p.y = 175;
            ship.position = p;
        }
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    for (UITouch *touch in touches) {
       
       if (fireRPressed) {
           SKAction *colorClear = [SKAction colorizeWithColor:[SKColor clearColor] colorBlendFactor:0 duration:.01];
           [fireButtonR runAction:colorClear];
           fireRPressed = NO;
       } else if (fireLPressed) {
           SKAction *colorClear = [SKAction colorizeWithColor:[SKColor clearColor] colorBlendFactor:0 duration:.01];
           [fireButtonL runAction:colorClear];
           fireLPressed = NO;
       }
    }
}

-(void)addAsteroid {
    //[Backgrounds update:currentTime];
    
}

-(void)fireBullets:(int)direction {
    SKSpriteNode *bulletfired = [bullets objectAtIndex:next_bullet++];
    if(next_bullet == bullets.count-1)
        next_bullet = 0;
    
    CGPoint bulletPosition;
    bulletPosition.y = ship.position.y + (ship.size.height / 2);
    if (direction) {
        //left
        bulletPosition.x = ship.position.x - 10;
    } else {
        //right
        bulletPosition.x = ship.position.x + 10;
    }
    
    bulletfired.position = bulletPosition;
    bulletfired.hidden = NO;
    CGPoint moveUP = CGPointMake(bulletPosition.x, self.frame.size.height);
    SKAction *bulletMoveAction = [SKAction moveTo:moveUP duration:0.65];
    SKAction *bulletDoneAction = [SKAction runBlock:(dispatch_block_t)^() {
        bulletfired.hidden = YES;
    }];
    //added bullet sound action
    SKAction *bulletFireSoundAction = [SKAction playSoundFileNamed:@"laser_ship.caf" waitForCompletion:NO];
    //sequence of actions to be performed
    SKAction *moveBulletActionWithDone = [SKAction sequence:@[bulletFireSoundAction, bulletMoveAction,bulletDoneAction]];
    [bulletfired runAction:moveBulletActionWithDone];
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    double curr_time = CACurrentMediaTime();
    if(curr_time > next_asteroid_spawn) {
        //double seconds = ((double)arc4random()/ARC4RANDOM_MAX)+0.20;
        double seconds = (arc4random() % 3);
        next_asteroid_spawn = curr_time + seconds;
        
        float randX = arc4random_uniform(self.frame.size.width);
        
        
        
        SKSpriteNode *asteroid = [asteroids objectAtIndex:next_asteroid++];
        if(next_asteroid == asteroids.count)
            next_asteroid = 0;
        
        float scaleSize = ((arc4random() % 3) + 2) / 10.0;
        float duration = arc4random_uniform(5)+10;
        
        if (scaleSize == 2) {
            duration = arc4random_uniform(5)+3;
        } else if (scaleSize == 4) {
            duration = arc4random_uniform(5)+17;
        }


        asteroid.position = CGPointMake(randX, self.frame.size.height + asteroid.size.height/2);
        asteroid.xScale = scaleSize;
        asteroid.yScale = scaleSize;
        asteroid.hidden = NO;
        
        CGPoint location = CGPointMake(randX, -self.frame.size.height-asteroid.size.height);
        SKAction *asteroidMove = [SKAction moveTo:location duration:duration];
        SKAction *asteroidDoneAction = [SKAction runBlock:(dispatch_block_t)^() { asteroid.hidden = YES;}];
        
        SKAction *moveAsteroidActionWithDone = [SKAction sequence:@ [asteroidMove, asteroidDoneAction]];
        [asteroid runAction:moveAsteroidActionWithDone];
    }
    
    for (SKSpriteNode *asteroid in asteroids) {
        if (asteroid.hidden) {
            continue;
        }
        for (SKSpriteNode *bullet in bullets) {
            if (bullet.hidden) {
                continue;
            }
            //when bullet hits asteroid
            if ([bullet intersectsNode:asteroid]) {
                SKAction *asteroidExplosionSound = [SKAction playSoundFileNamed:@"explosion_small.caf" waitForCompletion:NO];
                [asteroid runAction:asteroidExplosionSound];
                bullet.hidden = YES;
                asteroid.hidden = YES;
                tscore += 5;
                [scoreLabel setText:[NSString stringWithFormat:@"Score: %d", tscore]];
                //NSLog(@"you just destroyed an asteroid");
                continue;
            }
        }
        
        //Check if Asteroid intersects ship
        if([ship intersectsNode:asteroid]) {
            SKAction *shipExplosionSound = [SKAction playSoundFileNamed:@"explosion_large.caf" waitForCompletion:NO];
            [ship runAction:[SKAction sequence:@[shipExplosionSound]]];
            asteroid.hidden= YES;
            
            int i = shiplives.count-1;
            if(i >= 0 && lives >=0){
                SKSpriteNode *shiplive = [shiplives objectAtIndex:i];
                shiplive.hidden = YES;
                [shiplive removeFromParent];
                [shiplives removeObjectAtIndex:i];
                SKAction *blink = [SKAction sequence:@[[SKAction fadeOutWithDuration:0.1],
                                                       [SKAction fadeInWithDuration:0.1]]];
                SKAction *blinkForTime = [SKAction repeatAction:blink count:4];
                [ship runAction:blinkForTime];
                lives--;
            }
            else {
                //once all lives are over, should remove all the nodes from parent
                [ship removeFromParent];
                for(SKSpriteNode *asteroid in asteroids)
                    [asteroid removeFromParent];
                [fireButtonL removeFromParent];
                [fireButtonR removeFromParent];
                [scoreLabel removeFromParent];
                [myLabel removeFromParent];
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"OOPS ! No More Lives"
                                                               message: @"End of Play"
                                                              delegate: self
                                                     cancelButtonTitle:@ "Reset"
                                                     otherButtonTitles:@"Exit",nil];
                [alert show];
            }
            //NSLog(@"your ship has been hit!");
        }
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != [alertView cancelButtonIndex]) {
        exit(0);
    }
    else
        [self didMoveToView:self.view];
}

@end
