//
//  GameScene.h
//  p04-Desu
//

//  Copyright (c) 2016 Anuroop Desu. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameScene : SKScene
{
    BOOL fireLPressed;
    BOOL fireRPressed;
    BOOL touchesBegan;
    NSTimer *shipTimer;
}

@end
