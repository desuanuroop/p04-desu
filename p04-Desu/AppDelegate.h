//
//  AppDelegate.h
//  p04-Desu
//
//  Created by Anuroop Desu on 2/17/16.
//  Copyright © 2016 Anuroop Desu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

